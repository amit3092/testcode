const express = require('express');
const app = express();
const bodyParser = require('body-parser');

const mongodb = require('./mongodb.js');
const config = require('./config.js');
var db

app.use(bodyParser.urlencoded({extended:true}));

mongodb.connectToServer( function( err ) {
  app.listen(config.server.port, function() {
     console.log('Node server listening on ' + config.server.port);
     db = mongodb.getDb();
  })
});
app.get('/User/get', function(req, res){
  db.collection('Users').find().toArray(function(err, results) {
  res.send(results);
  })
  res.set({
    'Cache-Control': 'no-cache'
  });
});
app.get('/User/getSelected', function(req, res){
  db.collection('Users').find().toArray(function(err, results) {
  res.send(results);
  })
  res.set({
    'Cache-Control': 'no-cache'
  });
});
app.use(bodyParser.json())

app.post('/User/create', (req, res) => {
  //var myobj = { FirstName: req.FirstName, LastName: req.LastName };
  db.collection('Users').save(req, (err, result) => {
    if (err) return console.log(err)
    res.send('User created!');
  })
})
app.delete('/User/delete', (req, res) => {
  db.collection('Users').findOneAndDelete({name: req.body.name},
  (err, result) => {
    if (err) return res.send(500, err)
    res.send('User deleted!')
  })
})
app.get('/Book/get', function(req, res){
  db.collection('Books').find().toArray(function(err, results) {
  res.send(results);
  })
  res.set({
    'Cache-Control': 'no-cache'
  });
});
app.get('/Book/getSelected', function(req, res){
  db.collection('Books').find().toArray(function(err, results) {
  res.send(results);
  })
  res.set({
    'Cache-Control': 'no-cache'
  });
});
app.use(bodyParser.json())

app.post('/Book/create', (req, res) => {
  //var myobj = { FirstName: req.FirstName, LastName: req.LastName };
  db.collection('Books').save(req, (err, result) => {
    if (err) return console.log(err)
    res.send('User created!');
  })
})
app.delete('/Book/delete', (req, res) => {
  db.collection('Books').findOneAndDelete({name: req.body.name},
  (err, result) => {
    if (err) return res.send(500, err)
    res.send('User deleted!')
  })
})
